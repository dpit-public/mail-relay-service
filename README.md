# Mail Relay Service

This service accepts emails via a REST API and relays them to an SMTP Mail-Server. Custom auth logic can be integrated by implementing a Python class.

## Dev Setup

```bash
sudo apt update
sudo apt install python3-venv python3-pip
cd mail-relay-service
python3 -m venv .venv
source .venv/bin/activate
poetry install
```

## Configure and run

```bash
cp sample.env .env  # modify .env
docker-compose up --build -d
```

Open `http://localhost:8000` in your browser.

# Codestyle

```bash
isort .
black .
pycodestyle mail_relay_service
pylint mail_relay_service
```
