from fastapi import FastAPI

from .routes import add_routes
from .singletons import settings


app = FastAPI(
    debug=settings.debug,
    title="Mail Relay API",
    description="This service accepts emails via a REST API and relays them to an SMTP Mail-Server.",
    root_path=settings.root_path,
)
add_routes(app)
