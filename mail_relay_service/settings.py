from typing import Optional, Set

from pydantic import BaseSettings, DirectoryPath, EmailStr, conint


class Settings(BaseSettings):
    root_path: str = None
    cors_allow_origins: Set[str] = None
    debug: bool = False
    api_integration: str = None
    mail_username: str
    mail_password: str
    mail_port: int = 465
    mail_server: str
    mail_tls: bool = False
    mail_ssl: bool = True
    mail_debug: conint(gt=-1, lt=2) = 0
    mail_from: EmailStr
    mail_from_name: Optional[str] = None
    template_folder: Optional[DirectoryPath] = None
    suppress_send: conint(gt=-1, lt=2) = 0
    user_credentials: bool = True
    validate_certs: bool = True

    class Config:
        env_file = ".env"
        env_prefix = "mrs_"
