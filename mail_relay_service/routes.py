from fastapi.responses import JSONResponse
from fastapi_mail import MessageSchema
from fastapi_mail.fastmail import FastMail

from .singletons import api_integration, conf


def add_routes(app):
    @app.post("/email")
    async def _(message: MessageSchema, payload=api_integration.post_email_depends()):
        await api_integration.post_email_hook(payload=payload, message=message)

        fm = FastMail(conf)
        await fm.send_message(message)
        return JSONResponse(status_code=200, content={"message": "email has been sent"})
