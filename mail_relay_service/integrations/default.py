from fastapi import Depends, HTTPException, status

from ..settings import Settings


def _unauthorized():
    raise HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Integration not configured",
    )


class Default:
    def __init__(self, settings: Settings):
        self.settings = settings

    def post_email_depends(self):
        return Depends(_unauthorized)

    async def post_email_hook(self, payload, message):
        pass
