from fastapi import Depends

from ..settings import Settings


class DisableAuth:
    def __init__(self, settings: Settings):
        self.settings = settings

    def post_email_depends(self):
        return Depends()

    async def post_email_hook(self, payload, message):
        pass
