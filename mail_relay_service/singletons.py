from fastapi_mail import ConnectionConfig

from .api_integrations import get_api_integration
from .settings import Settings

settings = Settings()

conf = ConnectionConfig(
    MAIL_USERNAME=settings.mail_username,
    MAIL_PASSWORD=settings.mail_password,
    MAIL_PORT=settings.mail_port,
    MAIL_SERVER=settings.mail_server,
    MAIL_TLS=settings.mail_tls,
    MAIL_SSL=settings.mail_ssl,
    MAIL_DEBUG=settings.mail_debug,
    MAIL_FROM=settings.mail_from,
    MAIL_FROM_NAME=settings.mail_from_name,
    TEMPLATE_FOLDER=settings.template_folder,
    SUPPRESS_SEND=settings.suppress_send,
    USE_CREDENTIALS=settings.user_credentials,
    VALIDATE_CERTS=settings.validate_certs,
)

api_integration = get_api_integration(settings=settings)
