import importlib

from .integrations.default import Default
from .settings import Settings


def get_api_integration(settings: Settings):
    if settings.api_integration:
        module_name, class_name = settings.api_integration.split(":")
        module = importlib.import_module(module_name)
        IntegrationClass = getattr(module, class_name)
        return IntegrationClass(settings=Settings)

    return Default(settings=settings)
